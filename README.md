# Android_S01_Task

**Goal:**  
Build a movies discovery app where user can discover, search, rate and add-to-list movie(s). You may use [MoviesDB](https://developers.themoviedb.org/3) API.

**Modules:**  

`1- Home`

> A. Search:  
-  When a user taps on search bar, search history list should be shown (if available) with the ability to clear search history.
-  Search results should be shown after user taps “Search” whether its a button or a keyboard done key.
-  Clicking on a movie should take the user to “Movie Details” screen.

> B. Discover:  
-  This is where you list random movies categorized by genre (i.e. Drama, Comedy, etc.)
-  Each item -movie- in the list should only show name and poster.
-  Max value is 7 movies per genre.
-  There should be “See All” button in each genre section.
-  When a user taps “See All” button, show a new screen with all movies (name and poster) for this genre in an infinite scrolling.
-  Tapping on a movie should take the user to “Movie Details” screen.

`2- Movie Details`

> A. Movie details:
- User can see the movie name, short description, rating and poster.
- User can add/remove the movie to/from watch list which will be accessible in “Watch List” screen.
- User should see an indicator showing if this movie is already added to the watch list.
 
`3- Watch List`
- List of all saved movies (name and poster) in an invite scrolling.
- Tapping on a movie should take the user to “Movie Details” screen.

**Please make sure to:**
- [ ] Use offline storage (i.e. Room or Realm) for the watch list and search history.
- [ ] Add unit tests.
- [ ] Use MVVM and Rx.
- [ ] Follow Dependency Injection best practices.
- [ ] Implement neat and elegant UI with good UX that fit nicely on different screens sizes
- [ ] Handle loading, error and empty states.
- [ ] Support Android versions starting from 27
- [ ] Use any third-party library only if needed and be ready to explain why you used it.
- [ ] Create a new repo for this demo app and commit changes regularly as in real-world team work environment.
- [ ] Add read.me file to the repo with instructions of how-to instal and use your demo app.

**BONUS**
- Use offline storage to cache the results in “Home” screen for offline loading.
- Use CI flow.

_Happy coding!_


